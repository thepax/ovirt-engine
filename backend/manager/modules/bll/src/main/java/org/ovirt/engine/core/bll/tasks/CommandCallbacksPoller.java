package org.ovirt.engine.core.bll.tasks;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.ovirt.engine.core.bll.tasks.interfaces.CommandCallback;
import org.ovirt.engine.core.common.BackendService;
import org.ovirt.engine.core.common.businessentities.CommandEntity;
import org.ovirt.engine.core.common.config.Config;
import org.ovirt.engine.core.common.config.ConfigValues;
import org.ovirt.engine.core.compat.CommandStatus;
import org.ovirt.engine.core.compat.Guid;
import org.ovirt.engine.core.utils.CorrelationIdTracker;
import org.ovirt.engine.core.utils.timer.OnTimerMethodAnnotation;
import org.ovirt.engine.core.utils.timer.SchedulerUtilQuartzImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandCallbacksPoller implements BackendService {

    private static final Logger log = LoggerFactory.getLogger(CommandCallbacksPoller.class);
    private int pollingRate;

    @Inject
    private CommandsRepository commandsRepository;

    @Inject
    private SchedulerUtilQuartzImpl schedulerUtil;

    CommandCallbacksPoller() {
    }

    @PostConstruct
    private void init() {
        log.info("Start initializing {}", getClass().getSimpleName());
        pollingRate = Config.<Integer>getValue(ConfigValues.AsyncCommandPollingLoopInSeconds);
        initCommandExecutor();
        schedulerUtil.scheduleAFixedDelayJob(this,
                "invokeCallbackMethods",
                new Class[]{},
                new Object[]{},
                pollingRate,
                pollingRate,
                TimeUnit.SECONDS);
        log.info("Finished initializing {}", getClass().getSimpleName());
    }

    private boolean endCallback(Guid cmdId, CommandCallback callback, CommandStatus status) {
        try {
            if (status == CommandStatus.FAILED) {
                callback.onFailed(cmdId, getChildCommandIds(cmdId));
            } else {
                callback.onSucceeded(cmdId, getChildCommandIds(cmdId));
            }
        } catch (Throwable ex) {
            if (callback.shouldRepeatEndMethodsOnFail(cmdId)) {
                callback.finalizeCommand(cmdId, status == CommandStatus.SUCCEEDED);
                log.error("Failed invoking callback end method '{}' for command '{}' with exception '{}', the callback"
                        + " is marked for end method retries",
                        getCallbackMethod(status),
                        cmdId,
                        ex.getMessage());
                log.debug("Exception", ex);
                return true;
            }

            throw ex;
        }

        return false;
    }

    private List<Guid> getChildCommandIds(Guid cmdId) {
        return commandsRepository.getChildCommandIds(cmdId);
    }

    @OnTimerMethodAnnotation("invokeCallbackMethods")
    public void invokeCallbackMethods() {
        try {
            invokeCallbackMethodsImpl();
        } catch (Throwable t) {
            try {
                log.error("Exception in invokeCallbackMethods: {}", ExceptionUtils.getRootCauseMessage(t));
                log.debug("Exception", t);
            } catch (Throwable tt) {
                // log the stacktrace to the stdout as the exception was raised somewhere inside logging code
                tt.printStackTrace(System.out);
            }
        }
    }

    private void invokeCallbackMethodsImpl() {
        Iterator<Entry<Guid, CallbackTiming>> iterator = commandsRepository.getCallbacksTiming().entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<Guid, CallbackTiming> entry = iterator.next();

            Guid cmdId = entry.getKey();
            CallbackTiming callbackTiming = entry.getValue();
            CommandEntity commandEntity = commandsRepository.getCommandEntity(cmdId);
            CorrelationIdTracker.setCorrelationId(commandEntity != null
                    ? commandEntity.getCommandParameters().getCorrelationId() : null);
            if (commandEntity != null && updateCommandWaitingForEvent(commandEntity, callbackTiming)) {
                continue;
            } else {
                // Decrement counter; execute if it reaches 0
                callbackTiming.setRemainingDelay(callbackTiming.getRemainingDelay() - pollingRate);
                if (callbackTiming.getRemainingDelay() > 0) {
                    log.debug("The command {} has remaining delay {}, polling will be skipped.",
                            cmdId,
                            callbackTiming.getRemainingDelay());
                    continue;
                }
            }

            CommandCallback callback = callbackTiming.getCallback();
            CommandStatus status = commandsRepository.getCommandStatus(cmdId);
            log.debug("Command {} in status {}", cmdId, status);
            boolean runCallbackAgain = false;
            boolean errorInCallback = false;
            try {
                switch (status) {
                    case FAILED:
                    case SUCCEEDED:
                        runCallbackAgain = endCallback(cmdId, callback, status);
                        break;
                    case ACTIVE:
                        if (commandEntity == null) {
                            log.info("Not invoking command's {} doPolling method command entity is null, callback is {}.",
                                    cmdId,
                                    callbackTiming.getCallback() == null ? "NULL" : callbackTiming.getCallback().getClass().getCanonicalName());
                        } else if (commandEntity.isExecuted()) {
                            log.debug("Invoking command's {} doPolling method.", cmdId);
                            callback.doPolling(cmdId, getChildCommandIds(cmdId));
                        }
                        break;
                    case EXECUTION_FAILED:
                        if (callback.pollOnExecutionFailed()) {
                            log.debug("Invoking command's {} doPolling method.", cmdId);
                            callback.doPolling(cmdId, getChildCommandIds(cmdId));
                        } else {
                            log.info("Not invoking command's {} doPolling method callback's pollOnExecutionFailed is false.", cmdId);
                        }
                        break;
                    default:
                        break;
                }
            } catch (Throwable ex) {
                errorInCallback = true;
                log.info("Exception in invoking callback of command {}: {}",
                        cmdId,
                        ExceptionUtils.getRootCauseMessage(ex));
                log.debug("Exception", ex);
                handleError(ex, status, cmdId);
            } finally {
                if ((CommandStatus.FAILED == status || (CommandStatus.SUCCEEDED == status && !errorInCallback))
                        && !runCallbackAgain) {
                    log.debug("Callback of command {} has been notified, removing command from command repository.", cmdId);
                    commandsRepository.updateCallbackNotified(cmdId);
                    iterator.remove();
                    CommandEntity cmdEntity = commandsRepository.getCommandEntity(entry.getKey());
                    if (cmdEntity != null) {
                        // When a child finishes, its parent's callback should execute shortly thereafter
                        CallbackTiming rootCmdContainer =
                                commandsRepository.getCallbackTiming(cmdEntity.getRootCommandId());
                        if (rootCmdContainer != null) {
                            rootCmdContainer.setInitialDelay(pollingRate);
                            rootCmdContainer.setRemainingDelay(pollingRate);
                        }
                    }
                } else if (status != commandsRepository.getCommandStatus(cmdId)) {
                    log.debug("Command {} status {} has been updated to {}, command will be polled again.",
                            cmdId,
                            commandsRepository.getCommandStatus(cmdId),
                            status);
                    callbackTiming.setInitialDelay(pollingRate);
                    callbackTiming.setRemainingDelay(pollingRate);
                } else {
                    log.debug("Command {} will be polled again, updating initial and remaining delay.", cmdId);
                    int maxDelay = Config.<Integer>getValue(ConfigValues.AsyncCommandPollingRateInSeconds);
                    callbackTiming.setInitialDelay(Math.min(maxDelay, callbackTiming.getInitialDelay() * 2));
                    callbackTiming.setRemainingDelay(callbackTiming.getInitialDelay());
                }
            }
        }
        CorrelationIdTracker.setCorrelationId(null);
        commandsRepository.markExpiredCommandsAsFailure();
    }

    private void handleError(Throwable ex, CommandStatus status, Guid cmdId) {
        log.error("Error invoking callback method '{}' for '{}' command '{}'",
                getCallbackMethod(status),
                status,
                cmdId);
        log.error("Exception", ex);
        if (!CommandStatus.FAILED.equals(status)) {
            commandsRepository.updateCommandStatus(cmdId, CommandStatus.FAILED);
        }
    }

    private String getCallbackMethod(CommandStatus status) {
        switch (status) {
            case FAILED:
            case EXECUTION_FAILED:
                return "onFailed";
            case SUCCEEDED:
                return "onSucceeded";
            case ACTIVE:
                return "doPolling";
            default:
                return "Unknown";
        }
    }

    private void initCommandExecutor() {
        for (CommandEntity cmdEntity : commandsRepository.getCommands(true)) {
            if (!cmdEntity.isExecuted() &&
                    cmdEntity.getCommandStatus() != CommandStatus.FAILED &&
                    cmdEntity.getCommandStatus() != CommandStatus.EXECUTION_FAILED &&
                    cmdEntity.getCommandStatus() != CommandStatus.ENDED_WITH_FAILURE
                    ) {
                commandsRepository.updateCommandStatus(cmdEntity.getId(), CommandStatus.EXECUTION_FAILED);
            }

            if (!cmdEntity.isCallbackNotified()) {
                commandsRepository.addToCallbackMap(cmdEntity);
            }
        }
    }

    /**
     * Checks and updates the command if the time to wait for an event to arrive has expired, the command will be move
     * to polling mode
     *
     * @param cmdEntity
     *            the entity which represents the command
     * @param callbackTiming
     *            the container of the callback and its timing
     * @return {@code true} if command's callback is waiting for an event, else {@code false}
     */
    private boolean updateCommandWaitingForEvent(CommandEntity cmdEntity, CallbackTiming callbackTiming) {
        if (cmdEntity.isWaitingForEvent()) {
            if (callbackTiming.getWaitOnEventEndTime() < System.currentTimeMillis()) {
                log.info("The command '{}' reached its event's waiting timeout and will be moved to polling mode",
                        cmdEntity.getId());
                commandsRepository.removeEventSubscription(cmdEntity.getId());
                cmdEntity.setWaitingForEvent(false);
                return false;
            }
            log.debug("The command '{}' is waiting for event and will not be polled until event or timeout",
                    cmdEntity.getId());
            return true;
        }
        return false;
    }
}
